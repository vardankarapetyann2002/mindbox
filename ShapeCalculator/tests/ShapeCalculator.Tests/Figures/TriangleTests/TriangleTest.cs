﻿using FluentAssertions;
using ShapeCalculator.Figures.Triangle;
using ShapeCalculator.Shared.ValueObjects;
using Xunit;

namespace ShapeCalculator.Tests.Figures.TriangleTests
{
    /// <summary>
    /// Тесты для треугольника
    /// </summary>
    public class TriangleTest
    {
        [Theory]
        [InlineData(3, 4, 5, "red", "metal")]
        [InlineData(4, 5, 6, "blue", "gold")]
        [InlineData(12, 14, 17, "white", "bronze")]
        public void Create_WithValidData_CreatesTriangle(double sideA, double sideB, double sideC, string color, string material)
        {
            // Arrange

            // Act
            var triangle = new Triangle(
                new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC));

            // Assert
            triangle.Color.Value.Should().Be(color);
            triangle.Material.Value.Should().Be(material);
            triangle.SideA.Value.Should().Be(sideA);
            triangle.SideB.Value.Should().Be(sideB);
            triangle.SideC.Value.Should().Be(sideC);
        }

        [Theory]
        [InlineData(0, 1, 1)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 0)]
        [InlineData(-1, 1, 1)]
        [InlineData(1, -1, 1)]
        [InlineData(1, 1, -1)]
        public void Create_WithInvalidSide_ThrowsException(double sideA, double sideB, double sideC)
        {
            // Arrange
            var color = "red";
            var material = "metal";

            // Act & Assert
            Assert.Throws<ArgumentException>(() => new Triangle(
                new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC)));
        }

        [Theory]
        [InlineData(1, 1, 3)]
        [InlineData(3, 1, 1)]
        [InlineData(1, 3, 1)]
        public void Create_WithInvalidTriangle_ThrowsException(double sideA, double sideB, double sideC)
        {
            // Arrange
            string color = "red";
            string material = "metal";

            // Act & Assert
            Assert.Throws<ArgumentException>(() => new Triangle(new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC)));
        }

        [Theory]
        [InlineData(3, 4, 5, "red", "metal", 6 )]
        [InlineData(4, 5, 6, "blue", "gold", 9.921567416492215)]
        [InlineData(12, 14, 17, "white", "bronze", 83.0267276243018)]
        public void CalculateArea_ReturnsCorrectArea(double sideA, double sideB, double sideC, string color, string material, double expectedArea)
        {
            // Arrange
            var triangle = new Triangle(
                new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC));
            // Act
            var area = ShapeCalculator.CalculateArea(triangle);

            // Assert
            area.Should().Be(expectedArea);
        }

        [Theory]
        [InlineData(3, 4, 5, "red", "metal", 12)]
        [InlineData(4, 5, 6, "blue", "gold", 15)]
        [InlineData(12, 14, 17, "white", "bronze", 43)]
        public void CalculatePerimeter_ReturnsCorrectPerimeter(double sideA, double sideB, double sideC, string color, string material, double expectedPerimeter)
        {
            // Arrange
            var triangle = new Triangle(
                new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC));

            // Act
            var perimeter = ShapeCalculator.CalculatePerimeter(triangle);

            // Assert
            perimeter.Should().Be(expectedPerimeter);
        }

        [Theory]
        [InlineData(3, 4, 5)]
        [InlineData(5, 3, 4)]
        [InlineData(4, 5, 3)]
        public void IsRightTriangle_WithRightTriangle_ReturnsTrue(double sideA, double sideB, double sideC)
        {
            // Arrange
            string color = "red";
            string material = "metal";

            var triangle = new Triangle(
                new Color(color),
                new Material(material),
                new Side(sideA),
                new Side(sideB),
                new Side(sideC));

            // Act
            var isRight = triangle.IsRightTriangle();

            // Assert
            isRight.Should().BeTrue();
        }
    }
}
