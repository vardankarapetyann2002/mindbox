﻿using FluentAssertions;
using ShapeCalculator.Figures.Circle;
using ShapeCalculator.Shared.ValueObjects;
using Xunit;

namespace ShapeCalculator.Tests.Figures.CircleTests
{
    /// <summary>
    /// Тесты для круга.
    /// </summary>
    public class CircleTest
    {
        [Theory]
        [InlineData(5.0, "Red", "Steel")]
        [InlineData(14.2, "Black", "Golden")]
        [InlineData(5.0, "White", "Bronze")]
        public void CreateCircle_ShouldReturnValidCircle(double radius, string color, string material)
        {
            // Arrange

            // Act
            var circle = new Circle(new Radius(radius), new Color(color), new Material(material));

            // Assert
            circle.Radius.Value.Should().Be(radius);
            circle.Color.Value.Should().Be(color);
            circle.Material.Value.Should().Be(material);
        }

        [Theory]
        [InlineData(1.0, Math.PI)]
        [InlineData(2.0, 4 * Math.PI)]
        [InlineData(5.0, 25 * Math.PI)]
        public void CalculateArea_ShouldReturnCorrectArea(double radius, double expectedArea)
        {
            // Arrange
            var circle = new Circle(new Radius(radius), new Color("Red"), new Material("Steel"));

            // Act
            var actualArea = ShapeCalculator.CalculateArea(circle);

            // Assert
            actualArea.Should().Be(expectedArea);
        }

        [Theory]
        [InlineData(1.0, 2 * Math.PI)]
        [InlineData(2.0, 4 * Math.PI)]
        [InlineData(5.0, 10 * Math.PI)]
        public void CalculatePerimeter_ShouldReturnCorrectPerimeter(double radius, double expectedPerimeter)
        {
            // Arrange
            var circle = new Circle(new Radius(radius), new Color("Red"), new Material("Steel"));

            // Act
            var actualPerimeter = ShapeCalculator.CalculatePerimeter(circle);

            // Assert
            actualPerimeter.Should().Be(expectedPerimeter);
        }

        [Theory]
        [InlineData(-1.0, "Red", "Steel")]
        [InlineData(0.0, "Red", "Steel")]
        [InlineData(1.0, null, "Steel")]
        [InlineData(1.0, "", "Steel")]
        [InlineData(1.0, "Red", null)]
        [InlineData(1.0, "Red", "")]
        public void CreateCircle_WithInvalidArguments_ShouldThrowArgumentException(
            double radius, string color, string material)
        {
            // Arrange, Act and Assert
            Assert.Throws<ArgumentException>(() => new Circle(new Radius(radius), new Color(color), new Material(material)));
        }
    }
}
