﻿using ShapeCalculator.Abstractions;
using ShapeCalculator.Shared.ValueObjects;

namespace ShapeCalculator.Figures.Circle
{
    /// <summary>
    /// Круг
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// Радиус
        /// </summary>
        public Radius Radius { get; private set; }

        /// <summary>
        /// Цвет
        /// </summary>
        public Color Color { get; private set; }

        /// <summary>
        /// Материал
        /// </summary>
        public Material Material { get; private set; }

        public Circle(Radius radius, Color color, Material material)
        {
            Radius = radius;
            Color = color;
            Material = material;
        }

        /// <inheritdoc/>
        public override double CalculateArea()
        {
            return Math.PI * Radius.Value * Radius.Value;
        }

        /// <inheritdoc/>
        public override double CalculatePerimeter()
        {
            return 2 * Math.PI * Radius.Value;
        }
    }
}
