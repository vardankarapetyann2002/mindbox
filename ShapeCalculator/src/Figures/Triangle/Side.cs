﻿namespace ShapeCalculator.Figures.Triangle;

/// <summary>
/// Сторона
/// </summary>
public record Side
{
    public double Value { get; }

    public Side(double value)
    {

        if (value < 1)
        {
            throw new ArgumentException("Сторона не может быть меньше 1", nameof(value));
        }

        Value = value;
    }
}
