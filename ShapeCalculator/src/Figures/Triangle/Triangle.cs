﻿using ShapeCalculator.Abstractions;
using ShapeCalculator.Shared.ValueObjects;

namespace ShapeCalculator.Figures.Triangle
{
    public class Triangle : Shape, ITriangle
    {
        /// <summary>
        /// Цвет
        /// </summary>
        public Color Color { get; private set; }

        /// <summary>
        /// Материал
        /// </summary>
        public Material Material { get; private set; }

        /// <summary>
        /// Сторона A
        /// </summary>
        public Side SideA { get; private set; }

        /// <summary>
        /// Сторона B
        /// </summary>
        public Side SideB { get; private set; }

        /// <summary>
        /// Сторона C
        /// </summary>
        public Side SideC { get; private set; }

        public Triangle(Color color, Material material, Side sideA, Side sideB, Side sideC)
        {
            if (!(sideA.Value + sideB.Value > sideC.Value
                && sideA.Value + sideC.Value > sideB.Value
                && sideB.Value + sideC.Value > sideA.Value))
            {
                throw new ArgumentException($"Не является валидным треуголником");
            }


            Color = color;
            Material = material;
            SideA = sideA;
            SideB = sideB;
            SideC = sideC;
        }

        /// <inheritdoc/>
        public override double CalculateArea()
        {
            double p = (SideA.Value + SideB.Value + SideC.Value) / 2;
            return Math.Sqrt(p * (p - SideA.Value) * (p - SideB.Value) * (p - SideC.Value));
        }

        /// <inheritdoc/>
        public override double CalculatePerimeter()
        {
            return SideA.Value + SideB.Value + SideC.Value;
        }

        /// <inheritdoc/>
        public override string PrintDescription()
        {
            return
                $"Сторона А = {SideA.Value}, Сторона B = {SideB.Value}, Сторона B = {SideB.Value}, {Color.Value
                } цвета и сделанный из {Material.Value}";
        }

        /// <inheritdoc/>
        public bool IsRightTriangle()
        {
            double aSquared = Math.Pow(SideA.Value, 2);
            double bSquared = Math.Pow(SideB.Value, 2);
            double cSquared = Math.Pow(SideC.Value, 2);

            return AreDoublesEqual(aSquared + bSquared, cSquared, 0.0000000000000001) ||
                   AreDoublesEqual(aSquared + cSquared,bSquared, 0.0000000000000001) ||
                   AreDoublesEqual(bSquared + cSquared,aSquared, 0.0000000000000001);
        }

        private bool AreDoublesEqual(double a, double b, double tolerance)
        {
            return Math.Abs(a - b) <= tolerance;
        }
    }
}
