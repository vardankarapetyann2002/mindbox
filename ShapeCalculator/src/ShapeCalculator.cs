﻿using ShapeCalculator.Abstractions;

namespace ShapeCalculator
{
    /// <summary>
    /// Статический класс для вычислений
    /// </summary>
    public static class ShapeCalculator
    {
        /// <summary>
        /// Вычисление площади
        /// </summary>
        /// <param name="shape">Фигура</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static double CalculateArea(Shape shape)
        {
            if (shape == null)
            {
                throw new ArgumentNullException(nameof(shape), "Фигура не может быть null");
            }

            return shape.CalculateArea();
        }

        /// <summary>
        /// Вычисление периметра
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static double CalculatePerimeter(Shape shape)
        {
            if (shape == null)
            {
                throw new ArgumentNullException(nameof(shape), "Фигура не может быть null");
            }

            return shape.CalculatePerimeter();
        }
    }
}
