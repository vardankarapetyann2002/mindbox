﻿namespace ShapeCalculator.Shared.ValueObjects;

/// <summary>
/// Цвет
/// </summary>
public record Color
{
    public string Value { get; }

    public Color(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            throw new ArgumentException("Цвет не может быть пустым.", nameof(value));
        }

        if (value.Length > 60)
        {
            throw new ArgumentException("Длина цвета не может превышать 60 символов", nameof(value));
        }

        Value = value;
    }
}
