﻿namespace ShapeCalculator.Shared.ValueObjects;

/// <summary>
/// Радиус
/// </summary>
public record Radius
{
    public double Value { get; }

    public Radius(double value)
    {

        if (value < 1)
        {
            throw new ArgumentException("Радиус не может быть меньше 1", nameof(value));
        }

        Value = value;
    }
}
