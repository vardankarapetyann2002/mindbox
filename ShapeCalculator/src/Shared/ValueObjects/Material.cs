﻿namespace ShapeCalculator.Shared.ValueObjects;

/// <summary>
/// Материал
/// </summary>
public record Material
{
    public string Value { get; }

    public Material(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            throw new ArgumentException("Название материала не может быть пустым.", nameof(value));
        }

        if (value.Length > 60)
        {
            throw new ArgumentException("Длина названия материала не может превышать 60 символов", nameof(value));
        }

        Value = value;
    }
}

