﻿namespace ShapeCalculator.Abstractions
{
    /// <summary>
    /// Интерфейс треугольника
    /// </summary>
    public interface ITriangle
    {
        /// <summary>
        /// Проверку на то, является ли треугольник прямоугольным
        /// </summary>
        /// <returns></returns>
        public bool IsRightTriangle();
    }
}
