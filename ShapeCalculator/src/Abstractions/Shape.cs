﻿namespace ShapeCalculator.Abstractions
{
    /// <summary>
    /// Базовый класс фигуры
    /// </summary>
    public abstract class Shape
    {
        /// <summary>
        /// Метод для нахождения площади
        /// </summary>
        /// <returns></returns>
        public abstract double CalculateArea();

        /// <summary>
        /// Метод для нахождения периметра
        /// </summary>
        /// <returns></returns>
        public abstract double CalculatePerimeter();

        /// <summary>
        /// Описание фигуры
        /// </summary>
        public virtual string PrintDescription()
        {
            return "Простая фигура";
        }
    }
}
